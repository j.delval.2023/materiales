#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Program to retrieve practices
"""

import argparse
import json
import os
import subprocess
import urllib.request
import urllib.parse

from shutil import copyfile, copytree

from git.repo.base import Repo


#def add_api (practices):
#    for number, practice in practices.items():
#        practice['repo_api'] = practice['repo'].replace('/', '%2F')

practices = {
    'primos': {
        'repo': 'cursoprogram/plantillas/primos',
#        'solved_dir': '/home/jgb/docencia/isam-ptavi/practicas-gitlab/2021-2022/ptavi-p2-21-resuelta'
    },
    'triangulo': {
        'repo': 'cursoprogram/plantillas/triangulo',
#        'solved_dir': '/home/jgb/docencia/isam-ptavi/practicas-gitlab/2021-2022/ptavi-p2-21-resuelta'
    },
    'ordenpalabras': {
        'repo': 'cursoprogram/plantillas/ordenpalabras',
#        'solved_dir': '/home/jgb/docencia/isam-ptavi/practicas-gitlab/2021-2022/ptavi-p2-21-resuelta'
    },
    'ordenpalabras_ins': {
        'repo': 'cursoprogram/plantillas/ordenpalabras_ins',
#        'solved_dir': '/home/jgb/docencia/isam-ptavi/practicas-gitlab/2021-2022/ptavi-p2-21-resuelta'
    },
    'aproximacion': {
        'repo': 'cursoprogram/plantillas/aproximacion',
    },
    'cantidades': {
        'repo': 'cursoprogram/plantillas/cantidades',
    },
    'buscapalabras': {
        'repo': 'cursoprogram/plantillas/buscapalabras',
    },
    'final': {
        'repo': 'cursoprogram/plantillas/final',
    },
}


#add_api(practices)

def get_token() -> str:
    try:
        with open('token', 'r') as token_file:
            token: str = token_file.readline().rstrip()
            return token
    except FileNotFoundError:
        return ''

# def get_forks(repo: str, token: str = ''):
#     headers = {}
#     if token != '':
#         headers['PRIVATE-TOKEN'] = token
#     print(headers)
#     req = urllib.request.Request(url=f"https://gitlab.etsit.urjc.es/api/v4/projects/{repo}/forks",
#                                  headers=headers)
#     contents = urllib.request.urlopen(req).read()
#     contents_str = contents.decode('utf8')
#     return json.loads(contents_str)

def get_forks(repo: str, token: str = ''):
    req_headers = {}
    if token != '':
        req_headers['PRIVATE-TOKEN'] = token
    # Pages are ints starting in 1, so these are just initialization values
    this_page, total_pages = 1, None
    forks = []
    while (total_pages is None) or (this_page <= total_pages):
        url = f"https://gitlab.etsit.urjc.es/api/v4/projects/{repo}/forks?per_page=50&page={this_page}"
        req = urllib.request.Request(url=url, headers=req_headers)
        with urllib.request.urlopen(req) as response:
            contents = response.read()
            resp_headers = response.info()
            total_pages = int(resp_headers['x-total-pages'])
            this_page += 1
            contents_str = contents.decode('utf8')
            forks = forks + json.loads(contents_str)
    return forks



def clone(url, dir, token=''):
#    auth_url = url.replace('https://', f"https://Api Read Access:{token}@", 1)
    auth_url = url.replace('https://', f"https://jesus.gonzalez.barahona:{token}@", 1)
    print("Cloning:", dir, auth_url)
    Repo.clone_from(auth_url, os.path.join(testing_dir, dir))

def run_tests(dir: str, solved_dir: str, silent: bool=False):
    """Run tests for this directory"""
    print("Running tests for", dir)
    # Copy tests to evaltests in analyzed directory
    tests_dir = os.path.join(solved_dir, 'tests')
    copytree(tests_dir, os.path.join(dir, 'evaltests'))
    # Copy check.py to analyzed directory
    copyfile(os.path.join(solved_dir, 'check.py'), os.path.join(dir, 'check.py'))
    test_call = ['python3', 'check.py', '--silent', '--testsdir', 'evaltests']
    if silent:
        test_call.append('--silent')
        stderr = subprocess.PIPE
    else:
        stderr = None
    result = subprocess.run(test_call,
                            cwd=dir, stdout=subprocess.PIPE,
                            stderr=stderr, text=True)
#    print("Tests result:", result.stdout.splitlines()[-1])
    print("Tests result:", result.stdout)
    if result.returncode == 0:
        print(f"Running tests OK: {dir}")
        return True
    else:
        print(f"Running tests Error: {dir}")
        return False

def parse_args():
    parser = argparse.ArgumentParser(description='Evaluate practices.')
    parser.add_argument('--silent', action='store_true',
                        help="silent output, only summary is written")
    parser.add_argument('--no_clone', action='store_true',
                        help="don't clone repos, assume repos were already cloned")
    parser.add_argument('--practice', required=True,
                        help="practice number")
    parser.add_argument('--testing_dir', default='/tmp/p',
                        help="retrieval and testing directory")
    args = parser.parse_args()
    return(args)

if __name__ == "__main__":
    args = parse_args()
    practice_id = args.practice
    practice = practices[practice_id]
    testing_dir = args.testing_dir
    token: str = get_token()
    print("Token:", token)
    repo_api = urllib.parse.quote(practice['repo'], safe='')
    forks = get_forks(repo=repo_api, token=token)
#    print(forks)
    for fork in forks:
        # Each fork is a repo to analyze
        fork_data = {
            'url': fork['http_url_to_repo'],
            'name': fork['namespace']['name'],
            'path': fork['namespace']['path']
        }
        #print(fork_data)
        if not args.no_clone:
            clone(fork_data['url'], fork_data['path'], token)
        # Run tests in the cloned repo
#        print("About to run tests:", os.path.join(testing_dir, fork_data['path']))
#        run_tests(dir=os.path.join(testing_dir, fork_data['path']),
#                  solved_dir=practice['solved_dir'],
#                  silent=args.silent)
    print("Total forks:", len(forks))

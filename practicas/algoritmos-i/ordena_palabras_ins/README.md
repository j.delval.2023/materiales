### Ordenación de palabras mediante inserción

Realiza un programa, que se llame `sortwords.py`, que ordene alfabéticamente una lista de palabras que se le proporcionen como argumento en la línea de comandos, igual que en el ejercicio anterior, pero usando el método de inserción. Por ejemplo:

```commandline
python3 sortwords.py Hola adios Voy vengo
adios Hola vengo Voy
```

La lista de palabras se ordenará sin tener en cuenta mayúsculas o minúsculas (esto es, tanto la `a` como la `A` irán antes de la `b` o la `B`).  Utiliza una función para decidir si una cadena de caracteres es menor (está antes alfabéticamente) que otra. Utiliza ordenación por selección para ordenar las cadenas de caracteres.

El programa debe usar el esquema en él hay varias funciones:

* El programa principal está en la función `main`, como ya hemos hecho en otras prácticas.

* La ordenación la realiza una función `sort`, que recibe como parámetro la lista de palabras a ordenar, y produce como resultado la lista de palabras ordenada.

* A su vez, `sort` utiliza al función `sort_pivot`, que recibe como parámetros la lista de palabras a ordenar y la posición pivote. Esta función se encargará de colocar el elmento que esté en la posición pivote lo más a la izquierda que le toque, comparando con las palabras que estén a su izquierda hasta que encuentre una más pequeña (anterior en el diccionario).

* A su vez, `sort_pivot` utiliza la función `is_lower`, que compara dos palabras y devuelve un booleano indicando si la primera palabra es menor (anterior alfabéticamente) que la segunda, o no. Además, `is_lower` se encarga, al comparar caracteres, de pasarlos primero a minúsculas, para que la comparación sea independiente de si la letra es minúscula o mayúscula. Para hacer esto, puede usarse la función `lower`. Por ejemplo, para obtener una versión en minúscula de la letra `A`, puede usarse `A.lower()`. Si las variables `letra` y `letramin` son de tipo `string`, puede usarse `letramin = letra.lower()` para tener en `letramin` la letra que haya en `letra`, pero pasada a minúscula si era mayúscula. 

* La función `main` también usará la función `show`, que recibe como parámetro una lista de palabras, y la muestra en pantalla en el formato que se ve más arriba en el ejemplo.

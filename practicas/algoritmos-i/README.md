# Algoritmos I

## Ordenación

Actividades:

* Presentación: el problema de la ordenación de listas.

Ejercicios:

**Ejercicio:** Ordenar números mediante selección.
  * Ejercicio realizado en clase
  * [Enunciado](ordNumSeleccion/README.md)

**Ejercicio:** Ordenar caracteres mediante selección.
  * Ejercicio realizado en clase
  * [Enunciado](ordCharSeleccion/README.md)

**Ejercicio a entregar:** Ordenar formatos de imágenes por nivel de compresión.
  * **Fecha de entrega:** 2 de noviembre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/sortformats/
  * [Enunciado](sortformats/README.md).

**Ejercicio:** Ordenación de palabras mediante inserción.
  * Ejercicio recomendado
  * [Enunciado](ordena_palabras_ins/README.md)

**Ejercicio a entregar:** Ordenar formatos de imágenes (algoritmo por inserción).
  * **Fecha de entrega:** 8 de noviembre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/sortformats_ins/
  * [Enunciado](sortformats_ins/README.md).


**Referencias:**
* [Sorting Algorithms (Visualization)](https://www.cs.usfca.edu/~galles/visualization/ComparisonSort.html)
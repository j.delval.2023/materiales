# Ordenación Letras de Palabra mediante selección

Realiza un programa que ordene alfabéticamente los caracteres que estén en una cadena de caracteres (string) que se proporcione como argumento en la línea de comandos. Por ejemplo:

```commandline
python3 sortchars.py HolaQueTal
aaeHlloQTu
```

Utiliza el algoritmo de ordenación por selección.

Ten en cuenta que las cadenas de caracteres (strings) son inmutables (no pueden cambiar). Por eso, convendrá que antes de ordenar los caracteres, obtengas una lista de caracteres a partir del string. Por ejemplo, si la cadena se llama `word`, puedes usar:

```python
word_chars = list(word)
```

Para realizar la operación inversa (obtener una cadena de caracteres a partir de una lista de caracteres) puedes usar:

```python
word = ''.join(word_chars)
```

Solución: [sortchars.py](sortchars.py)
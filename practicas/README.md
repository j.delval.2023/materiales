## Informática I. Materiales de las prácticas de la asignatura

### Temas

* [Introducción](intro)
* [Entorno de programación I](entorno-i)
* [Entorno de programación II](entorno-ii)
* [Entorno de programación III](entorno-iii)
* [Algoritmos I](algoritmos-i)

* [Estructuras de control](control)
* [Estructura de un programa](estructura)
* [Estructura de datos](datos)

### Ejercicios a entregar

GitLab ([plantillas](https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023):

* 2023-11-02. Algoritmos I: Ordenar formatos de imágenes (algoritmo de inserción))
* 2023-10-31. Estructuras de datos: Compra en tienda
* 2023-10-26. Algoritmos I: Ordenar formatos de imágenes por nivel de compresión
* 2023-10-24. Estructuras de datos: Lista de la compra
* 2023-10-19. Estructura de un programa: Triángulo de números
* 2023-10-17. Entorno de programación III: Suma de números impares

Foro de ejercicios:

* 2023-10-10. Estructuras de control: Contador de contadores
* 2023-10-05. Estructuras de control: Animales
* 2023-09-25. Estructuras de control: Números pequeños, medianos y grandes 
* 2023-09-21. Entorno I: Comandos de Shell

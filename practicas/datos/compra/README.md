### Lista de la compra

Escribe un programa que mantenga una lista de la compra. Para escribirlo, hay que tener en cuenta que la lista se compondrá usando dos tipos de elementos:

* Compra habitual: serán los elementos que compramos siempre.
* Compra específico: serán los elementos que queremos comprar en esta ocasión.

El programa mantendrá la compra habitual en una tupla de strings (cademas de caracteres). La compra específica se mantendrá en una lista de strings, que se rellenará preguntando al usuario qué quiere comprar. Cuando el usuario haya especificado todo lo que quiere comprar, responderá a la pregunta con RETURN (cadena vacía). En ese momento, el programa  escribirá un listado de todo lo que se ha de comprar, tanto lo habitual como lo específico, y lo mostrará por pantalla (un elemento por línea), junto con el número total de elementos habituales, elementos específicos, y elementos totales. En el caso de que haya elementos repetidos, se eliminarán éstos, de forma que se muestre una lista de elementos sin repeticiones.

La tupla con la compra habitual será una variable llamada `habitual`, y la lista con la compra específica será una variable llamada `específica`. El programa se llamará `compra.py`, y tendrá la estructura que se presenta en el fichero `plantilla.py` del repositorio plantilla:

* La variable `habitual` tendrá la tupla con la compra habitual, y estará compuesta por los elementos "patatas", "leche" y "pan".
* La función `main` pedirá al usuario la compra específica, como se ha indicado, y escribirá en pantalla la lista de la compra total.

Por ejemplo, una ejecución típica será como sigue:

```commandline
python3 compra.py
Elemento a comprar: leche
Elemento a comprar: pan
Elemento a comprar:
Lista de la compra:
patatas
leche
pan
Elementos habituales: 3
Elementos específicos: 2
Elementos en lista: 3
```

Otro ejemplo:

```shell
python3 compra.py
Elemento a comprar: leche
Elemento a comprar: lentejas
Elemento a comprar: pan
Elemento a comprar:
Lista de la compra:
patatas
leche
pan
lentejas
Elementos habituales: 3
Elementos específicos: 3
Elementos en lista: 4
```

La cuenta de elementos habituales será la de la tupla correspondiente, la de elementos específicos la de todos los que proporcione el usuario, y la de elementos en lista, los que finalmente figuran en la lista.

Como verás en el fichero `plantilla.py`, en ese esquema, todo el código va en la función `main`, y el "programa principal" (el código que va después de `if __name__ ...`) servirá simplemente para llamar a `main`, que hará todo el trabajo. Para escribir tu programa te recomendamos que copies el contenido de `esquema.py` en `compra.py`, y completes luego `compra.py` hasta que funcione como se especifica en este enunciado.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`. El proceso resumido para hacerlo es el siguiente:

* Cuando hayas creado el repo bifurcado (fork) del repositorio plantilla, consigue su URL HTTPS de clonado, usando el botón "Clone" del repositorio en EIF GitLab.
* Utiliza, en tu ordenador, esa url para clonar el repositorio en un directorio local, que será el proyecto de PyCharm donde realizarás el ejercicio.
* Copia el código de `plantilla.py` a  `compra.py`, y modifica su contenido hasta que el programa funcione como debe.
* Borra del repositorio el programa `plantilla.py` que no tendrá que estar en el repositorio de entrega.
* Crea al menos dos commits, durante este proceso.
* Sube los commits a tu repositorio en EIF GitLab.

Al terminar, comprueba en EIF GitLab que el fichero `compra.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en EIF GitLab como "público" o "interno", para que podamos recoger la práctica.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_compra.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_compra.py
```

### Suma de números impares

Construir un programa que realice la suma de todos los números impares comprendidos entre dos números enteros no negativos introducidos por teclado por el usuario.

Al arrancar, el programa escribirá: `Dame un número entero no negativo: `. Cuando el usuario escriba el número, el programa escribirá: `Dame otro: `. A continuación, el programa mostrará por pantalla la suma de los números impares comprendidos entre esos dos números, incluidos cualquiera de ellos si es un número impar.

Llama al programa `impares.py`.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`. El proceso resumido para hacerlo es el siguiente:

* Cuando hayas creado el repo bifurcado (fork) del repositorio plantilla, consigue su URL HTTPS de clonado, usando el botón "Clone" del repositorio en EIF GitLab.

* Utiliza, en tu ordenador, esa url para clonar el repositorio en un directorio local. Si `<url>` es la URL que has obtenido de tu repositorio en GitLab:

```commandline
git clone <url>
```

* Eso te producirá un directorio que tendrá el mismo nombre que el final de la URL del repositorio, en este caso, `suma_impares`.

* Abre ese directorio como proyecto en GitLab, crea el fichero `impares.py`, y modifica su contenido hasta que el programa funcione como debe.

* Crea al menos un commit (versión) con PyCharm o con git en línea de comandos. Si lo haces desde la línea de comandos, tendrás que ejecutar (ojo con el punto al final, y con las comillas al principio y al final del comentario que quieras escribir):

```commandline
git add impares.py
git commit -m "Comentario de lo que hace el commit" .
```

Los comandos `git` los tendrás que ejecutar desde el directorio donde tienes tu repositiro git local, así que primero tendrás que haberte "cambiado" a él (`cd suma_impares` o algo parecido), o ejecutarlo desde el terminal de PyCharm para este proyecto.

* Sube el commit con PyCharm o con git en línea de comando a tu repositorio en EIF GitLab. Si lo haces desde la línea de comando:

```commandline
git push
```

Al terminar, comprueba en EIF GitLab que el fichero `triangulo.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en EIF GitLab como "público" o "interno", para que podamos recoger la práctica.

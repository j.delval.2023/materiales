# Estructura de un programa: divide y vencerás

Actividades:

* Presentación de transparencias del tema "Divide y vencerás"

Ejercicios:

**Ejercicio a entregar:** Triángulo de números

  * **Fecha de entrega:** 25 de octubre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/triangulo/
  * [Enunciado](triangulo/README.md).

### Números pequeños, medianos y grandes

Escribe un programa que pida un número entero. Si el número está entre 1 y 5 escribirá "número pequeño", si está entre 6 y 10 (incluids) escribirá "número mediano", y si es mayor que 10 escribirá "número grade". Cuando lo haya hecho, volverá a pedir otro número, y así sucesivamente. Terminará cuando el usuario escriba un 0.

Solución: [numeros_clasificados.py](numeros_clasificados.py)

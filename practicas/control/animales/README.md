### Animales clasificados

Escribe un programa que clasifique animales de acuerdo a dos características: grandes y pequeños por un lado, y rápidos y lentos por otro. Elige tú mismo los animales que clasificas (al menos seis, entre los que tendrá que haber al menos uno lento y pequeño, otro lento y grande, otro rápido y pequeño, y otro rápido y grande). El programa pedirá el nombre del animal, y escribirá si es grande o pequeño, y rápido o lento. Si no es uno de los animales que el programa considera, indicará "animal desconocido". Cuando esto haya terminado, se volverá a pedir otro nombre de animal, y así sucesivamente. Si en lugar de animal se pulsa simplemente la tecla "RETURN", el programa termina. La ejecución del programa será como sigue:

```shell
$ python3 animales.py
Dime un animal: elefante
Es un animal rápido y grande
Dime un animal: gato
No conozco a ese animal
Dime un animal: 
¡Hasta luego""
```

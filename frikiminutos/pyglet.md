## Pyglet

El módulo `pyglet` permite gestionar ventanas, teclado, ratón, y mostrar texto, imágenes, audio y video. Es útil, por ejemplo, para programar videojuegos o programas multimedia.

Pueden verse algunos ejemplos clonando el repo de pyglet:

```commandline
git clone https://github.com/pyglet/pyglet.git
cd pyglet
python examples/hello_world.py
python game/version5/asteroid.py
```

* Ejemplo muy sencillo: [hello.py](pyglet/hello.py)

* Dependencias: [pyglet](https://pypi.org/project/pyglet/)

```commandline
pip install pyglet
```

* [Sitio web de Pyglet](https://pyglet.org/)
* [Programming guide](https://pyglet.readthedocs.io/en/latest/programming_guide/installation.html)
* [In-depth game example](https://pyglet.readthedocs.io/en/latest/programming_guide/examplegame.html)

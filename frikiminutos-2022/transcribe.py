#!/usr/bin/env python3

import os

import whisper

model = whisper.load_model('tiny')
transcription = model.transcribe(os.path.join('misc', 'recording.wav'))
print(transcription['text'])
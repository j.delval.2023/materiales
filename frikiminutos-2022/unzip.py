#!/usr/bin/env python3

from zipfile import ZipFile

with ZipFile('fichero.zip') as zipfile:
    # List files in zipfile
    files_in_zipfile = zipfile.namelist()
    print(files_in_zipfile)
    # Extract files in zipfile
    zipfile.extractall()

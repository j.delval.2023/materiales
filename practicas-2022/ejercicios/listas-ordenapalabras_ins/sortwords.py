#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

import sys


def is_lower(first: str, second: str):
    """Return True if firs is lower (alphabetically) than second, when lowercased

    `<` is only used on single characteres.
    """

    lower = False
    for pos in range(len(first)):
        if pos < len(second):
            if first[pos].lower() < second[pos].lower():
                lower = True
                break
            if first[pos].lower() > second[pos].lower():
                break
    return lower

def sort_pivot(words: list, pos: int):
    current: int = pos
    while (current > 0) and (is_lower(words[current], words[current-1])):
        # Intercambia las palabras
        words[current], words[current-1] = words[current-1], words[current]
        current = current - 1

def sort(words: list):
    """Return the list of words, ordered alphabetically"""

    for pivot_pos in range(1, len(words)):
        sort_pivot(words, pivot_pos)

def show(words: list):

    for word in words:
        print(word, end=' ')
    print()

def main():
    words: list = sys.argv[1:]
    sort(words)
    show(words)

if __name__ == '__main__':
    main()

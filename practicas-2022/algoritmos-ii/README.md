# Algoritmos II

## Frikiminutos Python:

* [Grabación de audio](../../frikiminutos-2022/README.md#soundrecord).

## Búsqueda binaria

Actividades:

* Presentación: el problema de la búsqueda en listas ordenadas.

**Ejercicio:** "Búsqueda binaria en lista de números"

Realiza un programa que busque la posición (índice) de un número dado en una lista de números que se le proporcionen como argumentos en la línea de comandos. Por ejemplo:

```commandline
python3 search.py 9 4 6 9 13 15 17
2
```

El primer número que el programa recibe como argumento (en este ejemplo, el 9) será el número a buscar. El resto de los números serán la lista donde hay que buscarlo (en este caso, 4, 6, 9, 13, 15, 17. El resultado que muestra el programa es el número de órden (índice, empezando por 0) del número buscado dentro de esta lista.

Para realizar la búsqueda de un número n en una lista l, se utilizará el algortmo básico de búsqueda binaria:

* Comenzamos fijando un pivote izquierdo y un pivote derecho, que se inicializan al índice menor de la lista (`0`) y al índice mayor (`len(l)-1`)).
* Mientras no hayamos encontrado el número buscado:
  * Fijamos un índice de prueba que sea el índice medio entre el pivote izquierdo y el derecho, y vemos cuál es el valor del número en esa posición de la lista (número en índice de prueba).
  * Si el número en el índice de prueba es el buscado, hemos terminaddo, y el valor buscado es el índice de prueba.
  * Si el número en el índice de prueba es mayor que el buscado, el índice del número buscado tiene que estar a su izquierda, así que asignamos al pivote derecho el valor del índice de prueba (no hace falta ya buscar a la derecha del índice de prueba).
  * Si el número en el índice de prueba es menor que el buscado, el índice del número buscado tiene que estar a su derecha, así que asignamos al pivote izquierdo el valor del índice de prueba (no hace falta ya buscar a la izquierda del índice de prueba).


Solución: [search.py](search.py)

**Ejercicio:** "Búsquda binaria en cadena de caracteres"

Realiza un programa que busque un carácter en una cadena de caracteres ordenada. Tanto el carácter como la cadena de caracteres (string) se le proporcionarán como argumentos en la línea de comandos. Tanto para la ordenación como para la búsqueda ignoraremos la diferencia entre letra minúscula y letra mayúscula. Por ejemplo:

```commandline
python3 searchchars.py f acDeFhz
4
```

Utiliza el mismo algoritmo de búsqueda binaria que en el ejercicio anterior.

<!--Solución: [searchchars.py](searchchars.py)-->

**Ejercicio:** "Búsquda binaria en cadena de caracteres sin ordenar"

Realiza un programa que busque un carácter en una cadena de caracteres sin ordenar. Tanto el carácter como la cadena de caracteres (string) se le proporcionarán como argumentos en la línea de comandos.  Tanto para la ordenación como para la búsqueda ignoraremos la diferencia entre letra minúscula y letra mayúscula. Por ejemplo:

```commandline
python3 searchchars2.py h HolaQueTal
3
```

En este ejemplo, la cadena, ordenada, queda como `aaeHlloQTu`.

Para resolver el ejercicio, primero ordenaremos la cadena (podemos utilizar el código de [sortchars.py](../algoritmos-i/sortchars.py)), y luego aplicamos el mismo algoritmo de búsqueda binaria que en el ejercicio anterior.

Recuerda que las cadenas de caracteres (strings) son inmutables (no pueden cambiar). Por eso, convendrá que antes de ordenar los caracteres, obtengas una lista de caracteres a partir del string. Por ejemplo, si la cadena se llama `word`, puedes usar:

Para realizar la operación inversa (obtener una cadena de caracteres a partir de una lista de caracteres) puedes usar:

```python
word = ''.join(word_chars)
```

```python
word_chars = list(word)
```

<!--Solución: [searchchars2.py](searchchars2.py)-->

**Ejercicio: "Búsquda binaria en cadena de caracteres sin ordenar, en dos ficheros"**

Escribe un programa que se comporte exactamente como el descrito en el ejercicio anterior, pero en dos ficheros:

* Uno de ellos tendrá una función, `sort_string()`, que aceptará como argumento una cadena de caracteres (string) y devolverá una cadena de caracteres con los mismos caracteres, pero ordenados. Si es preciso, tendrá también las funciones auxiliares que precise.

* El otro tendrá el programa principal, en la función `main()`, que se encargará de leer los argumentos de la línea de comandos, llamar a `sort_string()` para ordenar la cadena, y luego buscar el carácter pasado como parámetro en esa cadena ordenada.

<!--Solución: [searchchars3.py](searchchars3.py) y [sortchars.py](sortchars.py)-->

**Ejercicio a entregar:** "Búsqueda de palabras"

[Enunciado](../ejercicios/README.md#buscapalabras), incluyendo repositorio plantilla y fecha de entrega.


**Referencias:**

* [Binary search (Wikipedia)](https://en.wikipedia.org/wiki/Binary_search_algorithm)
* [How to do a binary search in Python](https://realpython.com/binary-search-python/#implementing-binary-search-in-python)
* [Visualizing searching in a sorted list](https://www.cs.usfca.edu/~galles/visualization/Search.html)
